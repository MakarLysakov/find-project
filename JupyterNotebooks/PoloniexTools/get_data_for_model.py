from datetime import datetime
import time
import pandas as pd
from Tools_v1 import poloniexTools

t1 = poloniexTools()
PRIME = pd.DataFrame(
    columns=["startTime", "low", "high", "open", "close", "amount", "quantity"]
)

while True:
    time.sleep(300)
    now = int(time.time())
    ENDTIME = str(datetime.fromtimestamp(now)).replace("-", "/")
    start = now - 300
    STARTTIME = str(datetime.fromtimestamp(start)).replace("-", "/")

    PRIME.loc[len(PRIME.index)] = t1.get_data(
        "MINUTE_5", starttime_p=str(STARTTIME), endtime_p=str(ENDTIME)
    ).iloc[0]
    if len(PRIME.index) > 200:
        PRIME = PRIME.drop(axis=0, index=0)

    PRIME.to_csv("fresh_data.csv", sep=";")
