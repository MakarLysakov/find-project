import hmac
import base64
import hashlib
import json
import uuid
import time
from datetime import datetime
import pandas as pd
import requests
from polosdk import RestClient


class PoloniexToolsFutures:
    """
    Класс для работы с фючерсными инструментами биржи Poloniex

    Атрибуты
    --------

    Методы
    ------
    getOrderList():
        Возвращает словарь с активными ордерами

    cancelAllOrders():
        Отправляет запрос на биржу Poloniex на закрытие всех ордеров
        Возвращает словарь с response с биржи

    createOrder(side, size, price):
        Отправляет запрос на биржу на выставление ордера
        Возвращает
    """

    def __init__(self) -> None:
        self.api_key = "****"
        self.api_secret = "****"
        self.api_passphrase = "****"

    def getOrderList(self) -> dict:
        """
        Возвращает словарь со всеми активными ордерами на бирже Poloniex, описанный в FIND-

        Параметры
        ---------


        Возвращаемое значение
        ---------------------
        dict
        """
        now = int(time.time() * 1000)
        str_to_sign = f"{now}GET/api/v1/orders?status=active"
        signature = base64.b64encode(
            hmac.new(
                self.api_secret.encode("utf-8"),
                str_to_sign.encode("utf-8"),
                hashlib.sha256,
            ).digest()
        )
        headers = {
            "PF-API-SIGN": signature,
            "PF-API-TIMESTAMP": str(now),
            "PF-API-KEY": self.api_key,
            "PF-API-PASSPHRASE": self.api_passphrase,
        }
        return requests.request(
            "get",
            "https://futures-api.poloniex.com/api/v1/orders?status=active",
            headers=headers,
        )

    def cancelAllOrders(self):
        """
        Завершает все открытые ордера на бирже Poloniex
        Возвращает response c биржи Poloniex, описанный в FIND-

        Параметры
        ---------


        Возвращаемое значение
        ---------------------
        dict
        """
        now = int(time.time() * 1000)
        str_to_sign = f"{now}DELETE/api/v1/orders?symbol=BTCUSDTPERP"
        signature = base64.b64encode(
            hmac.new(
                self.api_secret.encode("utf-8"),
                str_to_sign.encode("utf-8"),
                hashlib.sha256,
            ).digest()
        )
        headers = {
            "PF-API-SIGN": signature,
            "PF-API-TIMESTAMP": str(now),
            "PF-API-KEY": self.api_key,
            "PF-API-PASSPHRASE": self.api_passphrase,
        }
        return requests.request(
            "delete",
            "https://futures-api.poloniex.com/api/v1/orders?symbol=BTCUSDTPERP",
            headers=headers,
        )

    def createOrder(self, side: str, size: str, price: str) -> dict:
        """
        Отправляет запрос на биржу Poloniex, на выставление ордера
        Возвращает response с биржи Poloniex, описанный в FIND-

        Параметры
        ---------
        side : str
            Направление ордера только (buy/ sell)
        size : str
            Размер выставляемой позиции, только целые числа 1,2,3.. Еденица размера равна 0.0001 BTC
        price : str
            Цена, по которой выставляется заявка, например 14000


        Возвращаемое значение
        ---------------------
        dict
        """
        body = {
            "clientOid": str(uuid.uuid4()),
            "leverage": "3",
            "price": price,
            "side": side,
            "size": size,
            "symbol": "BTCUSDTPERP",
            "type": "limit",
        }
        now = int(time.time() * 1000)
        body_json = json.dumps(body)
        str_to_sign = f"{now}POST/api/v1/orders" + body_json
        signature = base64.b64encode(
            hmac.new(
                self.api_secret.encode("utf-8"),
                str_to_sign.encode("utf-8"),
                hashlib.sha256,
            ).digest()
        )
        headers = {
            "PF-API-SIGN": signature,
            "PF-API-TIMESTAMP": str(now),
            "PF-API-KEY": self.api_key,
            "PF-API-PASSPHRASE": self.api_passphrase,
            # specifying content type or using json=data in request
            "Content-Type": "application/json",
        }
        return requests.request(
            "post",
            "https://futures-api.poloniex.com/api/v1/orders",
            headers=headers,
            data=body_json,
        )


class poloniexTools:
    """
    Класс с инуструментами для работы с API Poloniex

    Атрибуты
    --------
    public_key : str
        публичный ключ для API Poloniex
    secret_key : str
        секретный ключ для API Poloniex

    Методы
    ------
    get_data(amount, inter):
        Возвращает заданное количество свеч с данного момента с заданым интервалом

    get_balance():
        Возвращает лист со словарями, содержащими валюты на счету
    """

    def __init__(self, public_key=None, secret_key=None) -> None:
        self.public_key = public_key
        self.secret_key = secret_key

    def get_data(
        self, inter: str, starttime_p: str, endtime_p: str
    ) -> pd.core.frame.DataFrame:
        """
        Возвращает заданное количество свеч с данного момента с заданым интервалом

        Параметры
        ---------
        inter : str
            задается интервал заданных свеч
        starttime_p : str
            задается начальная точка отсчета
        endtime_p : str
            задается конченая точка отсчета

        Возвращаемое значение
        ---------------------
        pd.core.frame.DataFrame
        """

        client = RestClient(self.public_key, self.secret_key)
        interval_pool = {
            "MINUTE_1": 60,
            "MINUTE_5": 300,
            "HOUR_1": 3_600,
            "DAY_1": 86_400,
            "WEEK_1": 604_800,
        }
        start_time = int(
            datetime.timestamp(datetime.strptime(starttime_p, "%Y/%m/%d %H:%M:%S"))
        )
        end_time = int(
            datetime.timestamp(datetime.strptime(endtime_p, "%Y/%m/%d %H:%M:%S"))
        )
        amount = (end_time - start_time) // interval_pool[inter]
        data = []
        max_req_count = 10
        for _ in range(amount // 100):
            j = 0
            while j < max_req_count:
                try:
                    response = client.markets().get_candles(
                        symbol="BTC_USDT",
                        interval=inter,
                        start_time=(end_time - 1 - 100 * interval_pool[inter]) * 1_000,
                        end_time=end_time * 1_000,
                    )
                except:
                    j += 1
                else:
                    end_time -= interval_pool[inter] * 100
                    for value in range(len(response) - 1, -1, -1):
                        data.append(response[value])
                    time.sleep(0.2)
                    j = max_req_count
        j = 0
        while j < max_req_count:
            try:
                response = client.markets().get_candles(
                    symbol="BTC_USDT",
                    interval=inter,
                    start_time=(end_time - 1 - amount % 100 * interval_pool[inter])
                    * 1_000,
                    end_time=end_time * 1_000,
                )
            except:
                j += 1
            else:
                j = max_req_count

        for value in range(len(response) - 1, -1, -1):
            data.append(response[value])

        data_frame = pd.DataFrame(
            data,
            columns=[
                "low",
                "high",
                "open",
                "close",
                "amount",
                "quantity",
                "buyTakerAmount",
                "buyTakerQuantity",
                "tradeCount",
                "ts",
                "weightedAverage",
                "interval",
                "startTime",
                "closeTime",
            ],
        )
        data_frame["startTime"] = list(
            map(
                datetime.fromtimestamp,
                map(lambda number: int(number) // 1000, data_frame["startTime"]),
            )
        )
        data_frame["closeTime"] = list(
            map(
                datetime.fromtimestamp,
                map(lambda number: int(number) // 1000, data_frame["closeTime"]),
            )
        )
        data_frame = data_frame.drop(
            data_frame.columns[[6, 7, 8, 9, 10, 11, 13]], axis=1
        )
        data_frame = data_frame[
            ["startTime", "low", "high", "open", "close", "amount", "quantity"]
        ]
        data_frame = data_frame.iloc[::-1].reset_index(drop=True)
        return data_frame

    def get_balance(self) -> list:
        """
        Возвращает лист со словарями, содержащими валюты на счету

        Параметры
        ---------------------
        None

        Возвращаемое значение
        ---------------------
        list
        """
        try:
            if self.public_key is not None and self.secret_key is not None:
                client = RestClient(self.public_key, self.secret_key)
                return client.accounts().get_balances()
            return "Пользователь должен быть авторизован"

        except:
            return "Не удалось получить информацию о балансе"

    def all_orders(self) -> list:
        """
        Возвращает лист со словарями, содержащими сделки

        Параметры
        ---------------------
        None

        Возвращаемое значение
        ---------------------
        list
        """
        try:
            client = RestClient(self.public_key, self.secret_key)
            response = client.orders().get_all()
            return response

        except:
            return "Не удалось получить информацию о всех ордерах"
