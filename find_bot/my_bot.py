import time
import telebot
import schedule
from telebot import types
from polosdk import RestClient
from poloniex import Poloniex

bot = telebot.TeleBot("6591387880:AAE1rLLQxpIsexR57uRCB1gBsuPAazoDI0g")
poloniex_keys = Poloniex('SAEBDOYH-Y5390F1X-GZ3FQSHZ-2ZLG4ITK',
                         '65ad974770af38369bc4a9d31ed31f83105cff2f2032d5dde8d0d6bcfa7bca691c8cecad694581710b4793561e87d1f4c850eecccd4ea485d36f18acf1a30699')

client = RestClient()
tReq = 20    # Параметр периодичности запросов в секундах
running = True


@bot.message_handler(commands=['start'])
def start(message):
    user_id = message.chat.id
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton("Курс BTC/USDT")
    btn2 = types.KeyboardButton("Начать работу бота")
    markup.add(btn1, btn2)
    bot.send_message(user_id, text=(f"Привет, {message.chat.first_name}!\n Я fINd-бот. "
                                    f"Подскажу, когда лучше открыть и закрыть сделку по торговой паре BTC/USDT на площадке Poloniex."), reply_markup=markup)


@bot.message_handler(commands=['stop'])
def stop(message):
    global running
    running = False

@bot.message_handler(content_types=['text'])
def button_message(message):
    global running
    user_id = message.chat.id
    response = client.markets().get_price('BTC_USDT')
    if message.text == "Курс BTC/USDT":
        bot.send_message(user_id, f'Курс BTC/USDT: {response["price"]}')
    elif message.text == "Начать работу бота":
        running = True
        while True:
            if running:
                # Заявка на покупку
                bot.send_message(user_id, f'Выставить заявку на покупку BUY по цене: {response["price"]}')
                init_value = float(response["price"])
                time.sleep(tReq)
                response = client.markets().get_price('BTC_USDT')
                bot.send_message(user_id, f'Закрыть заявку на покупку BUY по цене: {response["price"]}\n'
                                          f'Прибыльность от сделки {(init_value/float(response["price"]))}%')
            if running:
                # Заявка на продажу
                time.sleep(tReq)
                response = client.markets().get_price('BTC_USDT')
                bot.send_message(user_id, f'Выставить заявку на продажу SELL по цене: {response["price"]}')
                init_value = float(response["price"])
                time.sleep(tReq)
                response = client.markets().get_price('BTC_USDT')
                bot.send_message(user_id, f'Закрыть заявку на продажу SELL по цене: {response["price"]}\n'
                                          f'Прибыльность от сделки {(float(response["price"]) / init_value)}%')
                time.sleep(tReq)

            bot.send_message(user_id, text=("Работа бота окончена"))
            break



bot.polling()
schedule.every(20).seconds.do(button_message)



while True:
    schedule.run_pending()
    time.sleep(10)

